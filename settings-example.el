;; This file contains settings that depend on what kind of system you're using
;; and other customization that you might want to be different between devices.
;; If you want to use this example just rename it to settings.el
;; This file is loaded before config.org

(setq kovarist:battery-p ; This device has a battery
      kovarist:termux-p nil ; Emacs is not used on Termux (i.e. without GUI). Only GUI mode is properly implemented
      kovariszt:dired-single-buffer t ; Don't create a bunch of Dired buffers when switching between directories
      kovariszt:japanese-use-mozc nil ; Use the built-in Japanese input method instead of mozc
      kovariszt:selected-theme 'zenburn ; Use the Zenburn Theme
      kovariszt:daemonp t ; Emacs runs as a daemon
      kovariszt:hl-line-p t ; Highlight line point is on
      kovariszt:weather-locations '("London" "Tokyo")) ; Locations for the weather report.

;; Location settings for celestial-mode
(setq calendar-longitude 25.5
      calendar-latitude 17.5
      calendar-location-name "Some place")
