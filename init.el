;; -*- flycheck-disabled-checkers: (emacs-lisp-checkdoc); -*-
;;; Set garbage collection threshold to very high number so it gets called more infrequently while loading. (default value 800000)
(setq gc-cons-threshold (* 100 1000 1000))
;;; Don't pop up a buffer whenever there's a small warning in some package, like "dockstring too wide"
(setq warning-minimum-level :error)
;;; Set up package.el and melpa
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(require 'use-package)
;;; use-package profiling. Set to non-nil and use use-package-report for statistics
(setq use-package-compute-statistics nil)

;;; Install my themes
(use-package doom-themes :ensure t)
(use-package zenburn-theme :ensure t)

;;; Set up diminish and hydra for configuration
(use-package diminish
  :ensure t
  :init
  (diminish 'eldoc-mode)
  (diminish 'org-indent-mode)
  (diminish 'abbrev-mode))

(use-package hydra
  :ensure t)

;;; Customizable variables for adjusting the config to taste.
;;; I mainly add these if I change my opinions about certain settings reasonably often.

(defcustom kovariszt:dired-single-buffer t
  "Non-nil if Dired should not spawn new buffers automatically."
  :group 'settings
  :type 'boolean)
(defcustom kovariszt:japanese-use-mozc nil
  "Non-nil if japanese-mozc should be used as the default input method.
Use japanese otherwise"
  :group 'settings
  :type 'boolean)
(defcustom kovarist:termux-p nil
  "Non-nil if Emacs is used from within Termux."
  :group 'settings
  :type 'boolean)
(defcustom kovarist:battery-p nil
  "Non-nil if Emacs is used on a device with battery."
  :group 'settings
  :type 'boolean)
(defcustom kovariszt:selected-theme 'doom-outrun-electric
  "Set to the name of the theme that is to be loaded."
  :group 'settings
  :type 'symbol)
(defcustom kovariszt:daemonp t
  "Non-nil if Emacs is run as a daemon."
  :group 'settings
  :type 'boolean)
(defcustom kovariszt:hl-line-p nil
  "Non-nil if `global-hl-line-mode' should be on."
  :group 'settings
  :type 'boolean)
(defcustom kovariszt:weather-locations nil
  "Set to a list of locations for the weather."
  :group 'settings
  :type 'sexp)

;;; Make customize write customization to ~/.emacs.d/custom-vars.el so that init.el is less cluttered.
(setq custom-file (locate-user-emacs-file "custom-vars.el"))
(load custom-file 'noerror 'nomessage)

;;; Set the customizable variables defined before this by loading settings.el
(when (file-readable-p (locate-user-emacs-file "settings.el"))
  (load (locate-user-emacs-file "settings.el") 'noerror 'nomessage))

;;; Load my actual config
(when (file-readable-p (locate-user-emacs-file "config.org"))
  (org-babel-load-file (expand-file-name (locate-user-emacs-file "config.org"))))

;;; Load secret config file. Put any secret variables, such as passwords, api keys, etc in here.
(when (file-readable-p (locate-user-emacs-file "secret.org"))
  (org-babel-load-file (expand-file-name (locate-user-emacs-file "secret.org"))))

;;; load selected theme
(load-theme kovariszt:selected-theme)

;;; Monospace fonts for Japanese
;; (set-frame-font "Noto Sans Mono CJK JP-12")
;; (set-frame-font "Sarasa Mono J-12")

;;; Reset garbace collection threshold to a reasonable number
(setq gc-cons-threshold (* 2 1000 1000))
