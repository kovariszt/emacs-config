(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(custom-safe-themes
   '("e8bd9bbf6506afca133125b0be48b1f033b1c8647c628652ab7a2fe065c10ef0"
     "e4a702e262c3e3501dfe25091621fe12cd63c7845221687e36a79e17cf3a67e0"
     "113a135eb7a2ace6d9801469324f9f7624f8c696b72e3709feb7368b06ddaccc"
     "6e18353d35efc18952c57d3c7ef966cad563dc65a2bba0660b951d990e23fc07"
     "1f292969fc19ba45fbc6542ed54e58ab5ad3dbe41b70d8cb2d1f85c22d07e518"
     "4d5d11bfef87416d85673947e3ca3d3d5d985ad57b02a7bb2e32beaf785a100e"
     "7e377879cbd60c66b88e51fad480b3ab18d60847f31c435f15f5df18bdb18184"
     "e1f4f0158cd5a01a9d96f1f7cdcca8d6724d7d33267623cc433fe1c196848554"
     "4594d6b9753691142f02e67b8eb0fda7d12f6cc9f1299a49b819312d6addad1d"
     "77fff78cc13a2ff41ad0a8ba2f09e8efd3c7e16be20725606c095f9a19c24d3d"
     "0d2c5679b6d087686dcfd4d7e57ed8e8aedcccc7f1a478cd69704c02e4ee36fe"
     "7964b513f8a2bb14803e717e0ac0123f100fb92160dcf4a467f530868ebaae3e"
     "ffafb0e9f63935183713b204c11d22225008559fa62133a69848835f4f4a758c"
     "10e5d4cc0f67ed5cafac0f4252093d2119ee8b8cb449e7053273453c1a1eb7cc"
     "f64189544da6f16bab285747d04a92bd57c7e7813d8c24c30f382f087d460a33"
     "0c83e0b50946e39e237769ad368a08f2cd1c854ccbcd1a01d39fdce4d6f86478"
     "93011fe35859772a6766df8a4be817add8bfe105246173206478a0706f88b33d"
     "2078837f21ac3b0cc84167306fa1058e3199bbd12b6d5b56e3777a4125ff6851"
     "4b6cc3b60871e2f4f9a026a5c86df27905fb1b0e96277ff18a76a39ca53b82e1"
     "56044c5a9cc45b6ec45c0eb28df100d3f0a576f18eef33ff8ff5d32bac2d9700"
     "e8ceeba381ba723b59a9abc4961f41583112fc7dc0e886d9fc36fa1dc37b4079"
     "3061706fa92759264751c64950df09b285e3a2d3a9db771e99bcbb2f9b470037"
     "2b501400e19b1dd09d8b3708cefcb5227fda580754051a24e8abf3aff0601f87"
     "6a5584ee8de384f2d8b1a1c30ed5b8af1d00adcbdcd70ba1967898c265878acf"
     "0325a6b5eea7e5febae709dab35ec8648908af12cf2d2b569bedc8da0a3a81c1"
     "5c7720c63b729140ed88cf35413f36c728ab7c70f8cd8422d9ee1cedeb618de5"
     "b754d3a03c34cfba9ad7991380d26984ebd0761925773530e24d8dd8b6894738"
     "c5878086e65614424a84ad5c758b07e9edcf4c513e08a1c5b1533f313d1b17f1"
     "9013233028d9798f901e5e8efb31841c24c12444d3b6e92580080505d56fd392"
     "a6920ee8b55c441ada9a19a44e9048be3bfb1338d06fc41bce3819ac22e4b5a1"
     "d481904809c509641a1a1f1b1eb80b94c58c210145effc2631c1a7f2e4a2fdf4"
     "34cf3305b35e3a8132a0b1bdf2c67623bc2cb05b125f8d7d26bd51fd16d547ec"
     "7c28419e963b04bf7ad14f3d8f6655c078de75e4944843ef9522dbecfcd8717d"
     "571661a9d205cb32dfed5566019ad54f5bb3415d2d88f7ea1d00c7c794e70a36"
     "8d3ef5ff6273f2a552152c7febc40eabca26bae05bd12bc85062e2dc224cde9a"
     "c8c4baac2988652a760554e0e7ce11a0fe0f8468736be2b79355c9d9cc14b751"
     "7758a8b8912ef92e8950a4df461a4d510484b11df0d7195a8a3d003965127abc"
     "f5f80dd6588e59cfc3ce2f11568ff8296717a938edd448a947f9823a4e282b66"
     "350fef8767e45b0f81dd54c986ee6854857f27067bac88d2b1c2a6fa7fecb522"
     "3c08da65265d80a7c8fc99fe51df3697d0fa6786a58a477a1b22887b4f116f62"
     "2b20b4633721cc23869499012a69894293d49e147feeb833663fdc968f240873"
     "30d174000ea9cbddecd6cc695943afb7dba66b302a14f9db5dd65074e70cc744"
     "b9761a2e568bee658e0ff723dd620d844172943eb5ec4053e2b199c59e0bcc22"
     "8b0cf3eace61815944d65ca122b40e36658d173827b2f28aa2c83cc7c0754cac"
     "2721b06afaf1769ef63f942bf3e977f208f517b187f2526f0e57c1bd4a000350"
     "da75eceab6bea9298e04ce5b4b07349f8c02da305734f7c0c8c6af7b5eaa9738"
     "f053f92735d6d238461da8512b9c071a5ce3b9d972501f7a5e6682a90bf29725"
     "ff24d14f5f7d355f47d53fd016565ed128bf3af30eb7ce8cae307ee4fe7f3fd0"
     "df6dfd55673f40364b1970440f0b0cb8ba7149282cf415b81aaad2d98b0f0290"
     "f4d1b183465f2d29b7a2e9dbe87ccc20598e79738e5d29fc52ec8fb8c576fcfd"
     "4990532659bb6a285fee01ede3dfa1b1bdf302c5c3c8de9fad9b6bc63a9252f7"
     "2771ec93656faf267521dce9ffe1a6ad88cd0bea87aa0e8c4fc80bf355c58c1d"
     "e978b5106d203ba61eda3242317feff219f257f6300bd9b952726faf4c5dee7b"
     "dd4582661a1c6b865a33b89312c97a13a3885dc95992e2e5fc57456b4c545176"
     "48042425e84cd92184837e01d0b4fe9f912d875c43021c3bcb7eeb51f1be5710"
     "691d671429fa6c6d73098fc6ff05d4a14a323ea0a18787daeb93fde0e48ab18b"
     "a9eeab09d61fef94084a95f82557e147d9630fbbb82a837f971f83e66e21e5ad"
     "e14884c30d875c64f6a9cdd68fe87ef94385550cab4890182197b95d53a7cf40"
     "c1d5759fcb18b20fd95357dcd63ff90780283b14023422765d531330a3d3cec2"
     "32f22d075269daabc5e661299ca9a08716aa8cda7e85310b9625c434041916af"
     "dfb1c8b5bfa040b042b4ef660d0aab48ef2e89ee719a1f24a4629a0c5ed769e8"
     "02d422e5b99f54bd4516d4157060b874d14552fe613ea7047c4a5cfa1288cf4f"
     "8c7e832be864674c220f9a9361c851917a93f921fedb7717b1b5ece47690c098"
     "aec7b55f2a13307a55517fdf08438863d694550565dee23181d2ebd973ebd6b8"
     "7ec8fd456c0c117c99e3a3b16aaf09ed3fb91879f6601b1ea0eeaee9c6def5d9"
     "13096a9a6e75c7330c1bc500f30a8f4407bd618431c94aeab55c9855731a95e1"
     "9e36779f5244f7d715d206158a3dade839d4ccb17f6a2f0108bf8d476160a221"
     "8b148cf8154d34917dfc794b5d0fe65f21e9155977a36a5985f89c09a9669aa0"
     "456697e914823ee45365b843c89fbc79191fdbaff471b29aad9dcbe0ee1d5641"
     "6f1f6a1a3cff62cc860ad6e787151b9b8599f4471d40ed746ea2819fcd184e1a"
     "d6b934330450d9de1112cbb7617eaf929244d192c4ffb1b9e6b63ad574784aad"
     "4ade6b630ba8cbab10703b27fd05bb43aaf8a3e5ba8c2dc1ea4a2de5f8d45882"
     "4e2e42e9306813763e2e62f115da71b485458a36e8b4c24e17a2168c45c9cf9d"
     "dccf4a8f1aaf5f24d2ab63af1aa75fd9d535c83377f8e26380162e888be0c6a9"
     "b5fd9c7429d52190235f2383e47d340d7ff769f141cd8f9e7a4629a81abc6b19"
     "014cb63097fc7dbda3edf53eb09802237961cbb4c9e9abd705f23b86511b0a69"
     "09b833239444ac3230f591e35e3c28a4d78f1556b107bafe0eb32b5977204d93"
     "9fb561389e5ac5b9ead13a24fb4c2a3544910f67f12cfcfe77b75f36248017d0"
     "9d5124bef86c2348d7d4774ca384ae7b6027ff7f6eb3c401378e298ce605f83a"
     "f079ef5189f9738cf5a2b4507bcaf83138ad22d9c9e32a537d61c9aae25502ef"
     "18cf5d20a45ea1dff2e2ffd6fbcd15082f9aa9705011a3929e77129a971d1cb3"
     "f74e8d46790f3e07fbb4a2c5dafe2ade0d8f5abc9c203cd1c29c7d5110a85230"
     "9dccdccfeb236623d5c7cf0250a92308cf307afde4ebdaf173b59e8bbbae1828"
     "443e2c3c4dd44510f0ea8247b438e834188dc1c6fb80785d83ad3628eadf9294"
     "f366d4bc6d14dcac2963d45df51956b2409a15b770ec2f6d730e73ce0ca5c8a7"
     "8b6506330d63e7bc5fb940e7c177a010842ecdda6e1d1941ac5a81b13191020e"
     "b89a4f5916c29a235d0600ad5a0849b1c50fab16c2c518e1d98f0412367e7f97"
     "94a94c957cf4a3f8db5f12a7b7e8f3e68f686d76ae8ed6b82bd09f6e6430a32c"
     "cf9414f229f6df728eb2a5a9420d760673cca404fee9910551caf9c91cff3bfa"
     "9b54ba84f245a59af31f90bc78ed1240fca2f5a93f667ed54bbf6c6d71f664ac"
     "fe2539ccf78f28c519541e37dc77115c6c7c2efcec18b970b16e4a4d2cd9891d"
     "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6"
     "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6"
     "1278c5f263cdb064b5c86ab7aa0a76552082cf0189acf6df17269219ba496053"
     "4699e3a86b1863bbc695236036158d175a81f0f3ea504e2b7c71f8f7025e19e3"
     "353ffc8e6b53a91ac87b7e86bebc6796877a0b76ddfc15793e4d7880976132ae"
     "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4"
     "4b0e826f58b39e2ce2829fab8ca999bcdc076dec35187bf4e9a4b938cb5771dc"
     "c2aeb1bd4aa80f1e4f95746bda040aafb78b1808de07d340007ba898efa484f5"
     "4b6b6b0a44a40f3586f0f641c25340718c7c626cbf163a78b5a399fbe0226659"
     "333958c446e920f5c350c4b4016908c130c3b46d590af91e1e7e2a0611f1e8c5"
     "82ef0ab46e2e421c4bcbc891b9d80d98d090d9a43ae76eb6f199da6a0ce6a348"
     "266ecb1511fa3513ed7992e6cd461756a895dcc5fef2d378f165fed1c894a78c"
     "6c98bc9f39e8f8fd6da5b9c74a624cbb3782b4be8abae8fd84cbc43053d7c175"
     "cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e"
     "a7b20039f50e839626f8d6aa96df62afebb56a5bbd1192f557cb2efb5fcfb662"
     "0466adb5554ea3055d0353d363832446cd8be7b799c39839f387abb631ea0995"
     "84b14a0a41bb2728568d40c545280dbe7d6891221e7fbe7c2b1c54a3f5959289"
     "47db50ff66e35d3a440485357fb6acb767c100e135ccdf459060407f8baea7b2"
     "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63"
     "d268b67e0935b9ebc427cad88ded41e875abfcc27abd409726a92e55459e0d01"
     "f6665ce2f7f56c5ed5d91ed5e7f6acb66ce44d0ef4acfaa3a42c7cfe9e9a9013"
     "cf922a7a5c514fad79c483048257c5d8f242b21987af0db813d3f0b138dfaf53"
     "4f1d2476c290eaa5d9ab9d13b60f2c0f1c8fa7703596fa91b235db7f99a9441b"
     "1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf"
     "40b961730f8d3c63537d6c3e6601f15c6f6381b9239594c7bf80b7c6a94d3c24"
     "028c226411a386abc7f7a0fba1a2ebfae5fe69e2a816f54898df41a6a3412bb5"
     "613aedadd3b9e2554f39afe760708fc3285bf594f6447822dd29f947f0775d6c"
     "745d03d647c4b118f671c49214420639cb3af7152e81f132478ed1c649d4597d"
     "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13"
     "b5803dfb0e4b6b71f309606587dd88651efe0972a5be16ece6a958b197caeed8"
     "23c806e34594a583ea5bbf5adf9a964afe4f28b4467d28777bcba0d35aa0872e"
     "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2"
     "5784d048e5a985627520beb8a101561b502a191b52fa401139f4dd20acb07607"
     "3d54650e34fa27561eb81fc3ceed504970cc553cfd37f46e8a80ec32254a3ec3"
     "d6844d1e698d76ef048a53cefe713dbbe3af43a1362de81cdd3aefa3711eae0d"
     "e2c926ced58e48afc87f4415af9b7f7b58e62ec792659fcb626e8cba674d2065"
     "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b"
     "a0be7a38e2de974d1598cf247f607d5c1841dbcef1ccd97cded8bea95a7c7639"
     "0d01e1e300fcafa34ba35d5cf0a21b3b23bc4053d388e352ae6a901994597ab1"
     "a6e620c9decbea9cac46ea47541b31b3e20804a4646ca6da4cce105ee03e8d0e"
     "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476"
     "da53441eb1a2a6c50217ee685a850c259e9974a8fa60e899d393040b4b8cc922"
     "8621edcbfcf57e760b44950bb1787a444e03992cb5a32d0d9aec212ea1cd5234"
     "d47f868fd34613bd1fc11721fe055f26fd163426a299d45ce69bef1f109e1e71"
     "22a514f7051c7eac7f07112a217772f704531b136f00e2ccfaa2e2a456558d39"
     "f91395598d4cb3e2ae6a2db8527ceb83fed79dbaf007f435de3e91e5bda485fb"
     "da186cce19b5aed3f6a2316845583dbee76aea9255ea0da857d1c058ff003546"
     "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8"
     "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae"
     default))
 '(elcord-boring-buffers-regexp-list
   '("^ " "\\\\*Messages\\\\*" "\\\\*Ibuffer\\\\*" "\\\\*info\\\\*"
     "\\\\*Org Src"))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f") ("NEXT" . "#dc752f") ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7") ("OKAY" . "#4f97d7") ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f") ("DONE" . "#86dc2f") ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d") ("HACK" . "#b1951d") ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f") ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(initial-frame-alist '((fullscreen . maximized)))
 '(mini-modeline-r-format
   '("%e" mode-line-front-space mode-line-frame-identification
     mode-line-buffer-identification " " mode-line-position " "
     evil-mode-line-tag
     (:eval (string-trim (format-mode-line mode-line-modes)))
     mode-line-misc-info))
 '(package-selected-packages
   '(2048-game alarm-clock all-the-icons-ibuffer app-launcher asm-blox
	       beacon bind-key blackjack bookmark+ buffer-move
	       casual-calc casual-dired casual-ibuffer casual-info
	       casual-isearch celestial-mode-line clhs clj-refactor
	       clojars clojure-snippets cobol-mode company-irony
	       company-quickhelp consult-company consult-dir
	       consult-flyspell consult-lsp consult-yasnippet
	       crontab-mode cyberpunk-theme dashboard diminish
	       dired-du dired-launch dired-narrow dired-single
	       dired-subtree diredfl disk-usage display-wttr dmenu
	       doom-themes eaf elcord elpher embark-consult emms empv
	       epc ess esup expand-region exwm fancy-battery fcitx
	       flycheck-irony flyspell-correct fm-bookmarks forge
	       forge-mode geiser-guile geiser-mit geiser-racket go
	       haskell-mode hungry-delete imgur keycast
	       keychain-environment klondike lorem-ipsum lsp-java
	       lsp-ui marginalia mark-multiple meme mentor mines
	       minesweeper mingus mini-modeline mozc mpdel-embark
	       mpdmacs mpv multi-vterm nasm-mode nerd-icons-dired nov
	       orderless org-bullets org-download org-tree-slide pass
	       pdf-tools pkg-info popper popup-kill-ring quick-preview
	       racket-mode rainbow-delimiters rainbow-mode
	       restart-emacs roguel-ike simple-mpc sis sly smartparens
	       spaceline spacemacs-theme steam sudo-edit svg-clock
	       switch-window symon syncthing tag-edit-mode trashed
	       treemacs-icons-dired treemacs-magit treemacs-projectile
	       undo-tree vertico visual-fill-column wfnames wgrep
	       which-key window-numbering winum yahtzee
	       yasnippet-snippets zenburn-theme zoom-window ztree))
 '(package-vc-selected-packages
   '((tag-edit-mode :vc-backend Git :url
		    "https://github.com/defaultxr/tag-edit-mode")
     (tag-editor-mode :vc-backend Git :url
		      "https://github.com/defaultxr/tag-edit-mode")
     (app-launcher :vc-backend Git :url
		   "https://github.com/SebastienWae/app-launcher")
     (forge-mode :vc-backend Git :url
		 "https://github.com/qwattash/forge-mode")
     (bookmark+ :vc-backend Git :url
		"https://github.com/emacsmirror/bookmark-plus")
     (vc-use-package :vc-backend Git :url
		     "https://github.com/slotThe/vc-use-package")))
 '(safe-local-variable-values
   '((eval ispell-change-dictionary "de")
     (eval progn (kovariszt:line-breaks)
	   (ispell-change-dictionary "de"))
     (eval kovariszt:line-breaks)
     (eval progn (kovariszt:line-breaks)
	   (setq org-pretty-entities-include-sub-superscripts nil))
     (eval progn (kovariszt:visual-lines)
	   (ispell-change-dictionary "de_AT"))
     (org-pretty-entities-include-sub-superscripts)
     (org-pretty-entities-include-sub-superscripts . NIL)
     (eval progn (kovariszt:line-breaks)
	   (ispell-change-dictionary "de_AT"))
     (eval progn (ispell-change-dictionary "de_AT")
	   (kovariszt:visual-lines))
     (eval ispell-change-dictionary "de_AT")
     (eval kovariszt:visual-lines)
     (eval setq org-pretty-entities-include-sub-superscripts nil)
     (org-confirm-babel-evaluate)))
 '(symon-mode nil)
 '(tags-apropos-additional-actions '(("Common Lisp" clhs-doc clhs-symbols))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(window-numbering-face ((t (:foreground "cyan" :weight bold))) t))
